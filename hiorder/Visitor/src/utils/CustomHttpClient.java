package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import android.util.Log;


public class CustomHttpClient 
{
	private static HttpClient mclient;
	public static String executeHttpPost(String url,ArrayList<NameValuePair> postParameters) 
	{
		BufferedReader input = null;
		String result="";
		try
		{
			HttpClient client = getHttpClient();
			HttpPost request = new HttpPost(url);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			request.setEntity(formEntity);
			HttpResponse response = client.execute(request);
			input = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line="";
			
			String seprate = System.getProperty("line.separator");
			while((line=input.readLine())!=null)
			{
				result = result+line+seprate;
			}
			input.close();
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.d("postHttp", "can not post http request or response");
		}
		return result;
	}
	public static HttpClient getHttpClient()
	{
		if(mclient==null)		
		{
			mclient = new DefaultHttpClient();
			HttpParams param = mclient.getParams();
			HttpConnectionParams.setConnectionTimeout(param,CenterConstant.HTTP_TIMEOUT);
			HttpConnectionParams.setSoTimeout(param,CenterConstant.HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(param,CenterConstant.HTTP_TIMEOUT);
		}
		return mclient;
		
	}

}
