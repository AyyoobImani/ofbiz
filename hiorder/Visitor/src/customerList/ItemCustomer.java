package customerList;

import android.widget.ImageView;

public class ItemCustomer 
{
	private String name;
	private String phoneNumber;
	private ImageView picture;
	public ItemCustomer(String Name,String PhoneNumber , ImageView Picture )
	{
		super();
		this.name = Name;
		this.phoneNumber = PhoneNumber;
		this.picture =Picture;
	}
	public String getName()
	{
		return this.name;
	}
	public String getPhoneNumber()
	{
		return this.phoneNumber;
	}
	public ImageView getImage()
	{
		return this.picture;
	}
	public void setName(String Name)
	{
		this.name=Name;
	}
	public void setPhoneNumber(String phone)
	{
		this.phoneNumber = phone;
	}
	public void setImage(ImageView img)
	{
		this.picture = img;
	}
	
	

}
