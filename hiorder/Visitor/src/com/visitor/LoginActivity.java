package com.visitor;




import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import utils.CenterConstant;
import utils.CustomHttpClient;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity
{
	Button login ;
	EditText username_edit;
	EditText password_edit;
	
	String username;
	String password;
	
	//for debug: 
	final String tag= "LoginActivity";
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.login_activity);
		login = (Button) findViewById(R.id.login_button);
		username_edit = (EditText) findViewById(R.id.username_text_id);
		password_edit = (EditText) findViewById(R.id.password_text_id);
		username_edit.setSelection(0);
		
		
		addButtonListener();
		
	}
	// click listener in button.
	private void addButtonListener()
	{
		username_edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				
			}
		});
		login.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				Log.d(tag, "Login button pressed");
				ArrayList<NameValuePair> parameter = new ArrayList<NameValuePair>();
				parameter.add(new BasicNameValuePair("username", username));
				parameter.add(new BasicNameValuePair("password",password));
				try
				{
					String response;
					response = CustomHttpClient.executeHttpPost(CenterConstant.SERVER_URL+"Login", parameter);
					JSONArray jarray = new JSONArray(response);
					JSONObject jsonResult = jarray.getJSONObject(0);
					String result = jsonResult.getString("result");
					if(result=="OK")
					{
						//open main activity
					}
					else
					{
						AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
						dialog.setTitle("پیام خطا");
						dialog.setMessage(result);
						dialog.setIcon(R.drawable.ic_launcher);
						dialog.setPositiveButton("قبول",new DialogInterface.OnClickListener() {
							
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.cancel();
								
							}
						});
						AlertDialog alertDialog = dialog.create();
						alertDialog.show();
					}
				}
				catch(Exception except)
				{
					except.printStackTrace();
				}
				
				
			
				
			}
		});
		
	}
	

}
