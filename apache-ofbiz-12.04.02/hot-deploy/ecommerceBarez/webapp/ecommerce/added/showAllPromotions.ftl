<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<div class=" span9">
<form class="reg-page promote">
<div>
    <div class="reg-header"><h2> ${uiLabelMap.OrderSpecialOffers}</h2></div>
    <div>
        
        <#-- show promotions text -->
        <#list productPromosAllShowable as productPromo>
        <p>
          <div class="row">
            <div class="span1"> <a href="<@ofbizUrl>showPromotionDetails?productPromoId=${productPromo.productPromoId}</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.CommonDetails}</a></div>
            <div> class="span11">${StringUtil.wrapString(productPromo.promoText?if_exists)}</div>
          </div>
        </p>
        </#list>
        
    </div>
</div>
</form>
<#if (shoppingCartSize > 0)>
  ${screens.render(promoUseDetailsInlineScreen)}
</#if>
</div>