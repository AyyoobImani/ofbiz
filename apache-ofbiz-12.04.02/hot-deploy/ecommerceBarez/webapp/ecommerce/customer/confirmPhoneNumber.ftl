
<script type="text/javascript">
setTimeout(addResendLink,60000);
function addResendLink(){
	document.getElementById("resendBox").style.display = "block";
	
}

</script>

<div class="reg-page  halfwidth margin-right-220"  id="innerBox">
	<div class="screenlet-body-contactus">
		<form action="<@ofbizUrl>confirmPhoneNumber</@ofbizUrl>" name="getConfirmtionNumberForm" method="post" class="bottom-margin">
			<div><label for="randomNumber"> ${uiLabelMap.PartyReceivedVerificationCode}</label></div>
			<input type="text" name="randomNumber" id="randomNumber" />
			<input type="submit" class="btn btn-primary" value="${uiLabelMap.CommonSubmit}"/>
		</form>
		
		<div class="row " id="resendBox" style="display: none;">
			<form action="<@ofbizUrl>resendVerificationNumber</@ofbizUrl>" class="span3 reg-page promote center" id="resendVerificationCodeForm" nam="resendVerificationCodeForm">
				
				${uiLabelMap.PartyIfYouDidntGetVerificationCode}
				<input type="submit" value="${uiLabelMap.CommonClickHere}"/>
				${uiLabelMap.PartyToResendCode}
				<hr/>
				<p>${uiLabelMap.PartyCallUsIfCouldntGetSms}</p>
				<#list requestAttributes.phoneNumbers as phoneNumber>	 
					<p class="pagination-left">${phoneNumber.countryCode?if_exists} &nbsp;${phoneNumber.areaCode?if_exists} &nbsp;${phoneNumber.contactNumber?if_exists}</p>
				</#list>
			</form>
		</div>
	</div>
</div>
