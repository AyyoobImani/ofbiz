<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#if canNotView>
  <h3>${uiLabelMap.PartyContactInfoNotBelongToYou}.</h3>
  <a href="<@ofbizUrl>${donePage}</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.CommonBack}</a>
<#else>
  <#if !contactMech?exists>
    <#-- When creating a new contact mech, first select the type, then actually create -->
    <#if !requestParameters.preContactMechTypeId?exists && !preContactMechTypeId?exists>
    <div class="reg-page margin-right-220 halfwidth promote">
	    <div class="reg-header"><h2>${uiLabelMap.PartyCreateNewContactInfo}</h2></div>
	    <form method="post" action='<@ofbizUrl>editcontactmechnosave</@ofbizUrl>' name="createcontactmechform">
	      <div class="row">
		      <div class="span">${uiLabelMap.PartySelectContactType}:</div>
		      <div class="span">
			      <select name="preContactMechTypeId" class='selectBox'>
		             <#list contactMechTypes as contactMechType>
		               <option value='${contactMechType.contactMechTypeId}'>${contactMechType.get("description",locale)}</option>
		             </#list>
			      </select>
		      </div>
		   </div>
		   <div class="row">
		   <div class="span6 pagination-left">
	    	 <a href="javascript:document.createcontactmechform.submit()" class="btn btn-primary">${uiLabelMap.CommonCreate}</a>
	    	</div>
	    	</div>
	    </form>
    </div>
    <#-- <p><h3>ERROR: Contact information with ID "${contactMechId}" not found!</h3></p> -->
    </#if>
  </#if>

  <#if contactMechTypeId?exists>
    <div class="reg-page margin-right-220 halfwidth promote">
    <#if !contactMech?exists>
      <div class="reg-header"><h2>${uiLabelMap.PartyCreateNewContactInfo}</h2></div>
      
   
        <form method="post" action='<@ofbizUrl>${reqName}</@ofbizUrl>' name="editcontactmechform" id="editcontactmechform">
   
          <input type='hidden' name='contactMechTypeId' value='${contactMechTypeId}' />
          <#if cmNewPurposeTypeId?has_content><input type='hidden' name='contactMechPurposeTypeId' value='${cmNewPurposeTypeId}' /></#if>
          <#if preContactMechTypeId?has_content><input type='hidden' name='preContactMechTypeId' value='${preContactMechTypeId}' /></#if>
          <#if paymentMethodId?has_content><input type='hidden' name='paymentMethodId' value='${paymentMethodId}' /></#if>
    <#else>
      <div class="reg-header"><h2>${uiLabelMap.PartyEditContactInfo}</h2></div>      
      
      <form method="post" action='<@ofbizUrl>${reqName}</@ofbizUrl>' name="editcontactmechform" id="editcontactmechform">
      
         <input type="hidden" name="contactMechId" value='${contactMechId}' />
         <input type="hidden" name="contactMechTypeId" value='${contactMechTypeId}' />
    </#if>

    <#if contactMechTypeId = "POSTAL_ADDRESS">
    <input type="hidden" name="countryGeoId" value="IRN">
    
      <div class="row">
        <div class="span15">${uiLabelMap.PartyToName}</div>
        <div class="span">
          <input type="text" class='inputBox' size="30" maxlength="60" name="toName" value="${postalAddressData.toName?if_exists}" />
        </div>
      </div>
      
      
      <div class="row">
        <div class="span15">${uiLabelMap.PartyAddressLine1}*</div>
        <div class="span">
          <input type="text" class='inputBox' size="100" maxlength="100" name="address1" value="${postalAddressData.address1?if_exists}" />
        </div>
      </div>
      
      <div class="row">
        <div class="span15">${uiLabelMap.PartyAddressLine2}</div>
        <div class="span">
            <input type="text" class='inputBox' size="100" maxlength="100" name="address2" value="${postalAddressData.address2?if_exists}" />
        </div>
      </div>
      
      <div class="row">
        <div class="span15">${uiLabelMap.PartyCity}</div>
        <div class="span">
            <input type="text" class='inputBox' size="30" maxlength="30" name="city" value="${postalAddressData.city?if_exists}" />
        </div>
      </div>
      
      <div class="row">
        <div class="span15">${uiLabelMap.PartyZipCode}*</div>
        <div class="span">
          <input type="text" class='inputBox' size="12" maxlength="10" name="postalCode" value="${postalAddressData.postalCode?if_exists}" />
        </div>
      </div>
      
    <#elseif contactMechTypeId = "TELECOM_NUMBER">
      
      <div class="row">
        <div class="span15">${uiLabelMap.PartyPhoneNumber}</div>
        <div class="span">
          <input type="text" class='inputBox' size="15" maxlength="15" name="contactNumber" value="${telecomNumberData.contactNumber?if_exists}" />
        </div>
      </div>

    <#elseif contactMechTypeId = "EMAIL_ADDRESS">
      <div class="row">
        <div class="span15">${uiLabelMap.PartyEmailAddress}</div>
        <div class="span">
          <input type="text" class='inputBox' size="60" maxlength="255" name="emailAddress" value="<#if tryEntity>${contactMech.infoString?if_exists}<#else>${requestParameters.emailAddress?if_exists}</#if>" />
        </div>
      </div>
    <#else>
      <div class="row">
        <div class="span15">${contactMechType.get("description",locale)?if_exists}</div>
        <div class="span">
            <input type="text" class='inputBox' size="60" maxlength="255" name="infoString" value="${contactMechData.infoString?if_exists}" />
        </div>
      </div>
    </#if>
    <input type="hidden" name="DONE_PAGE" value="${donePage}" />  
    </form>
  

  <div class="row">
  		
      	<div class="span3"><a href='<@ofbizUrl>${donePage}</@ofbizUrl>' class="btn btn-primary">${uiLabelMap.CommonGoBack}</a></div>
      	<div class="span3 pagination-left"><a href="javascript:document.editcontactmechform.submit()" class="btn btn-primary">${uiLabelMap.CommonSave}</a></div>
  </div>
  <#else>
       
    <a href="<@ofbizUrl>${donePage}</@ofbizUrl>" class="btn btn-primary">${uiLabelMap.CommonGoBack}</a>
  </#if>
  </form>
</#if>