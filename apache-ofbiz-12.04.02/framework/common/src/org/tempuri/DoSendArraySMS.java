
package org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="uUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uCellphone" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="uMessage" type="{http://tempuri.org/}ArrayOfString" minOccurs="0"/>
 *         &lt;element name="uFarsi" type="{http://tempuri.org/}ArrayOfBoolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uUsername",
    "uPassword",
    "uNumber",
    "uCellphone",
    "uMessage",
    "uFarsi"
})
@XmlRootElement(name = "doSendArraySMS")
public class DoSendArraySMS {

    protected String uUsername;
    protected String uPassword;
    protected String uNumber;
    protected ArrayOfString uCellphone;
    protected ArrayOfString uMessage;
    protected ArrayOfBoolean uFarsi;

    /**
     * Gets the value of the uUsername property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUsername() {
        return uUsername;
    }

    /**
     * Sets the value of the uUsername property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUsername(String value) {
        this.uUsername = value;
    }

    /**
     * Gets the value of the uPassword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPassword() {
        return uPassword;
    }

    /**
     * Sets the value of the uPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPassword(String value) {
        this.uPassword = value;
    }

    /**
     * Gets the value of the uNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNumber() {
        return uNumber;
    }

    /**
     * Sets the value of the uNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNumber(String value) {
        this.uNumber = value;
    }

    /**
     * Gets the value of the uCellphone property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getUCellphone() {
        return uCellphone;
    }

    /**
     * Sets the value of the uCellphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setUCellphone(ArrayOfString value) {
        this.uCellphone = value;
    }

    /**
     * Gets the value of the uMessage property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getUMessage() {
        return uMessage;
    }

    /**
     * Sets the value of the uMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setUMessage(ArrayOfString value) {
        this.uMessage = value;
    }

    /**
     * Gets the value of the uFarsi property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBoolean }
     *     
     */
    public ArrayOfBoolean getUFarsi() {
        return uFarsi;
    }

    /**
     * Sets the value of the uFarsi property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBoolean }
     *     
     */
    public void setUFarsi(ArrayOfBoolean value) {
        this.uFarsi = value;
    }

}
