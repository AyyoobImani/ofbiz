
package org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DoReceiveSMSDSResponse }
     * 
     */
    public DoReceiveSMSDSResponse createDoReceiveSMSDSResponse() {
        return new DoReceiveSMSDSResponse();
    }

    /**
     * Create an instance of {@link DoSendArraySMSResponse }
     * 
     */
    public DoSendArraySMSResponse createDoSendArraySMSResponse() {
        return new DoSendArraySMSResponse();
    }

    /**
     * Create an instance of {@link DoReceiveSMSAllDS }
     * 
     */
    public DoReceiveSMSAllDS createDoReceiveSMSAllDS() {
        return new DoReceiveSMSAllDS();
    }

    /**
     * Create an instance of {@link ArrayOfBoolean }
     * 
     */
    public ArrayOfBoolean createArrayOfBoolean() {
        return new ArrayOfBoolean();
    }

    /**
     * Create an instance of {@link HelloWorldResponse }
     * 
     */
    public HelloWorldResponse createHelloWorldResponse() {
        return new HelloWorldResponse();
    }

    /**
     * Create an instance of {@link HelloWorld }
     * 
     */
    public HelloWorld createHelloWorld() {
        return new HelloWorld();
    }

    /**
     * Create an instance of {@link DoReceiveSMSDSResponse.DoReceiveSMSDSResult }
     * 
     */
    public DoReceiveSMSDSResponse.DoReceiveSMSDSResult createDoReceiveSMSDSResponseDoReceiveSMSDSResult() {
        return new DoReceiveSMSDSResponse.DoReceiveSMSDSResult();
    }

    /**
     * Create an instance of {@link DoReceiveSMS }
     * 
     */
    public DoReceiveSMS createDoReceiveSMS() {
        return new DoReceiveSMS();
    }

    /**
     * Create an instance of {@link DoReceiveSMSAllDSResponse }
     * 
     */
    public DoReceiveSMSAllDSResponse createDoReceiveSMSAllDSResponse() {
        return new DoReceiveSMSAllDSResponse();
    }

    /**
     * Create an instance of {@link DoReceiveSMSResponse }
     * 
     */
    public DoReceiveSMSResponse createDoReceiveSMSResponse() {
        return new DoReceiveSMSResponse();
    }

    /**
     * Create an instance of {@link DoSendArraySMS }
     * 
     */
    public DoSendArraySMS createDoSendArraySMS() {
        return new DoSendArraySMS();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link DoSendSMS }
     * 
     */
    public DoSendSMS createDoSendSMS() {
        return new DoSendSMS();
    }

    /**
     * Create an instance of {@link GetInfo }
     * 
     */
    public GetInfo createGetInfo() {
        return new GetInfo();
    }

    /**
     * Create an instance of {@link GetInfoResponse }
     * 
     */
    public GetInfoResponse createGetInfoResponse() {
        return new GetInfoResponse();
    }

    /**
     * Create an instance of {@link DoReceiveSMSAllDSResponse.DoReceiveSMSAllDSResult }
     * 
     */
    public DoReceiveSMSAllDSResponse.DoReceiveSMSAllDSResult createDoReceiveSMSAllDSResponseDoReceiveSMSAllDSResult() {
        return new DoReceiveSMSAllDSResponse.DoReceiveSMSAllDSResult();
    }

    /**
     * Create an instance of {@link DoReceiveSMSAll }
     * 
     */
    public DoReceiveSMSAll createDoReceiveSMSAll() {
        return new DoReceiveSMSAll();
    }

    /**
     * Create an instance of {@link DoReceiveSMSAllResponse }
     * 
     */
    public DoReceiveSMSAllResponse createDoReceiveSMSAllResponse() {
        return new DoReceiveSMSAllResponse();
    }

    /**
     * Create an instance of {@link DoSendSMSResponse }
     * 
     */
    public DoSendSMSResponse createDoSendSMSResponse() {
        return new DoSendSMSResponse();
    }

    /**
     * Create an instance of {@link GetDelivery }
     * 
     */
    public GetDelivery createGetDelivery() {
        return new GetDelivery();
    }

    /**
     * Create an instance of {@link DoReceiveSMSDS }
     * 
     */
    public DoReceiveSMSDS createDoReceiveSMSDS() {
        return new DoReceiveSMSDS();
    }

    /**
     * Create an instance of {@link GetDeliveryResponse }
     * 
     */
    public GetDeliveryResponse createGetDeliveryResponse() {
        return new GetDeliveryResponse();
    }

}
