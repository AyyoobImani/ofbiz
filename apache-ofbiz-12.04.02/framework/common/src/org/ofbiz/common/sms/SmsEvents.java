package org.ofbiz.common.sms;

import java.util.Locale;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilHttp;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ServiceAuthException;
import org.ofbiz.service.ServiceValidationException;



public class SmsEvents {
	public static final String module = SmsEvents.class.getName();
	public static final String resourceErr = "CommonErrorUiLabels";
	
	public static String sendOrderConfirmtion(HttpServletRequest request, HttpServletResponse response){
		
		HttpSession session = request.getSession();
		String orderId = (String) request.getAttribute("orderId");
		
		LocalDispatcher dispatcher = (LocalDispatcher) request.getAttribute("dispatcher");
		Locale locale = UtilHttp.getLocale(request);
		
		
		
		Map<String, Object> smsParams = FastMap.newInstance();
		
		GenericValue userLogin = (GenericValue) session.getAttribute("userLogin");
        smsParams.put("sendTo", userLogin.get("userLoginId"));
        smsParams.put("userName", "barezpakhsh");
        smsParams.put("sederNumber", "10001878");
        smsParams.put("content", "آقای "+session.getAttribute("autoName")+" یک سفارش با شماره "+orderId+"برای شما ثبت شد."+"\n بارزپخش");
        smsParams.put("isUTF8", "true");
        smsParams.put("password", "bpkerman");
		
        try {
			dispatcher.runAsync("sendSMS", smsParams);
		} catch (ServiceAuthException e) {
			Debug.logError("couldnt send sms " , module);
			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
			
			e.printStackTrace();
			return "error";
		} catch (ServiceValidationException e) {
			
			Debug.logError("couldnt send sms " , module);
			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
			e.printStackTrace();
			return "error";
		} catch (GenericServiceException e) {
			
			Debug.logError("couldnt send sms " , module);
			request.setAttribute("_ERROR_MESSAGE_", UtilProperties.getMessage(resourceErr, "smsEvents.error_accoured_when_trying_to_send_confirmtion_sms", locale));
			
			e.printStackTrace();
			return "error";
		}
        
		return "success";
	}

}
