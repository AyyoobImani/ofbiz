<#assign currentCatalogId = Static["org.ofbiz.product.catalog.CatalogWorker"].getCurrentCatalogId(request)/>
<div class="screenlet-body">
<label>${uiLabelMap.EcommerceSearch}</label>
    <form name="keywordsearchform" class="navbar-form pull-left form-search" id="keywordsearchbox_keywordsearchform" method="post" action="<@ofbizUrl>keywordsearch</@ofbizUrl>">
      <fieldset class="inline">
        <div class="input-append">
	      <input type="hidden" name="VIEW_SIZE" value="10" />
	      <input type="hidden" name="PAGING" value="Y" />
	      <input type="hidden" name="SEARCH_CATALOG_ID" value="${currentCatalogId}" />
	      <input type="hidden" name="SEARCH_SUB_CATEGORIES" value="Y" />
	      
          <input type="text" name="SEARCH_STRING" class="span2 search-query" size="14" maxlength="50" value="${requestParameters.SEARCH_STRING?if_exists}">
		    <button type="submit" class="btn"><i class="icon-search"></i></button>
		  </input>
        </div>
      </fieldset>
    </form></div>